# <%= moduleName %>

[![npm](https://img.shields.io/npm/v/<%= moduleName %>.svg)](https://www.npmjs.com/package/<%= moduleName %>) <% if (git.resource === "github.com") { %>[![David](https://img.shields.io/david/<%= git.owner %>/<%= git.name %>.svg)](https://david-dm.org/<%= git.owner %>/<%= git.name %>) [![Build Status](https://travis-ci.org/<%= git.owner %>/<%= git.name %>.svg?branch=master)](https://travis-ci.org/<%= git.owner %>/<%= git.name %>)<% } %> <% if (git.resource === "gitlab.com") { %>[![Build Status](http://gitlab.com/<%= git.owner %>/<%= git.name %>/badges/master/build.svg)](https://gitlab.com/<%= git.owner %>/<%= git.name %>/builds)<% } %>

<%= description %>
