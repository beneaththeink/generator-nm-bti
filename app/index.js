"use strict";

var yeoman = require('yeoman-generator');
var parseGitUrl = require('git-url-parse');
var autoreleaseSetup = require("autorelease-setup");

module.exports = yeoman.generators.Base.extend({
	prompting: function () {
		var cb = this.async();

		this.prompt([{
			type: "input",
			name: "moduleName",
			message: "What do you want to name your module?",
			default: this.appname.replace(/\s/g, "-"),
			validate: Boolean,
			filter: function (val) {
				return val.replace(/\s+/g, "-");
			}
		}, {
			type: "input",
			name: "description",
			message: "Describe your module.",
			validate: Boolean
		}, {
			type: "confirm",
			name: "private",
			message: "Is this a private module?",
			default: false
		}, {
			type: "input",
			name: "repositoryUrl",
			message: "What is the repository url?",
			validate: Boolean
		}, {
			type: "confirm",
			name: "license",
			message: "Is this licensed under MIT?",
			default: true
		}, {
			type: "checkbox",
			name: "targets",
			message: "Which platforms are you targeting?",
			choices: [
				{ "value": "node", "name": "Node.js" },
				{ "value": "browser", "name": "Browser" }
			],
			"default": [ "node" ]
		}, {
			type: "confirm",
			name: "cli",
			message: "Does this module include a CLI tool?",
			default: false,
			when: function(props) {
				return ~props.targets.indexOf("node") || !~props.targets.indexOf("browser");
			}
		}, {
			type: "confirm",
			name: "sass",
			message: "Does this module need to build Sass files?",
			default: false
		}, {
			type: "confirm",
			name: "autorelease",
			message: "Publish this package with autorelease?",
			default: true
		}], (props) => {
			this.templateData = Object.assign({
				cli: false
			}, props, {
				name: this.user.git.name(),
				email: this.user.git.email(),
				git: parseGitUrl(props.repositoryUrl)
			});

			cb();
		});
	},
	configuring: function () {
		const copy = (f, t) => {
			this.fs.copyTpl(this.templatePath(f), this.destinationPath(t), this.templateData);
		};

		var tpl = this.templateData;
		copy('_package.json', 'package.json');
		copy("test/", "test");
		copy("src/index.js", "src/index.js");
		copy('gitignore', '.gitignore');
		copy('npmignore', '.npmignore');
		copy('eslintrc', '.eslintrc.json');
		copy('readme.md', 'README.md');
		copy('Makefile', 'Makefile');
		if (tpl.license) copy('license', 'LICENSE');

		if (tpl.cli) {
			copy("src/cli.js", "src/cli.js");
		}

		let node = ~tpl.targets.indexOf("node");
		let browser = ~tpl.targets.indexOf("browser");

		if (tpl.sass) {
			copy("build/importer.js", "build/importer.js");
			copy("styles/", "styles");
		}

		if (node && browser) {
			copy("build/rollup.browser.js", "build/rollup.browser.js");
			copy("build/rollup.node.js", "build/rollup.node.js");
		} else if (browser) {
			copy("build/rollup.browser.js", "rollup.config.js");
		} else {
			copy("build/rollup.node.js", "rollup.config.js");
		}
	},
	install: function() {
		var tpl = this.templateData;
		var cb = this.async();

		if (tpl.repositoryUrl) {
			this.spawnCommandSync("git", [ "init" ]);
			this.spawnCommandSync("git", [ "remote", "add", "origin", tpl.git.toString("ssh") ]);
		}

		const mods = [
			"tape",
			"eslint",
			"babel-eslint",
			"rollup",
			"rollup-plugin-babel",
			"rollup-plugin-json",
			"babel-plugin-transform-object-rest-spread"
		];

		const browser = ~tpl.targets.indexOf("browser");
		const node = ~tpl.targets.indexOf("node");

		if (node) {
			mods.push(
				"babel-plugin-external-helpers",
				"babel-plugin-transform-async-to-generator",
				"babel-plugin-transform-es2015-destructuring",
				"babel-plugin-transform-es2015-parameters"
			);
		}

		if (browser || !node) {
			mods.push("babel-preset-es2015-rollup");
		}

		if (browser) {
			mods.push(
				"rollup-plugin-commonjs",
				"rollup-plugin-node-resolve",
				"rollup-plugin-node-builtins"
			);
		}

		if (tpl.sass) {
			mods.push(
				"node-sass@3.4.2",
				"autoprefixer",
				"postcss-cli"
			);
		}

		if (tpl.cli) {
			mods.push("minimist");
		}

		const done = () => {
			if (browser) {
				this.npmInstall([
					"rollup-plugin-node-globals@1.0.3"
				], {
					saveDev: true,
					saveExact: true
				});
			}

			this.npmInstall(mods, {
				saveDev: true
			});

			cb();
		};

		if (tpl.autorelease) {
			autoreleaseSetup().then(done).catch(cb);
		} else {
			done();
		}
	},
	end: function() {
		this.spawnCommand("npm", [ "run", "build" ]);
	}
});
